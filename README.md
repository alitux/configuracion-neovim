## Neovim para Python

Configuración de NeoVim orientada a la programación con Python.

Los requisitos mínimos son los siguientes: 

1. Vim-Plug
2. Git
3. nvim
4. jedi

Basado en el trabajo de Joaquín Varela (https://github.com/JoakoV3/nvim-windows10)
